import {Component, OnInit} from '@angular/core';
import {TaskService} from '../task.service';
import {TaskModel} from '../task.model';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  tasks: TaskModel[];

  constructor(private taskService: TaskService) {}

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('tasks'))) {
      this.tasks = JSON.parse(localStorage.getItem('tasks'));
    } else {
      this.taskService.getTasks()
        .subscribe((data: TaskModel[]) => {
          this.tasks = data.filter((task) => {
            return task.obj_status === 'active';
          });
          localStorage.setItem('tasks', JSON.stringify(this.tasks));
        });
    }
  }
}
