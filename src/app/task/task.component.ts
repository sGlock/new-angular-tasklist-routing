import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {TaskService} from '../task.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit, OnDestroy {

  @ViewChild('editTaskInput') editTaskInput: ElementRef;

  id: number;
  name: string;
  tags: string;
  actualEffort: number;
  estimatedEffort: number;
  dueDate: string;
  description: string;

  editTaskStatus = false;
  editTaskValue = '';
  tempTasks = [];
  sub: Subscription;

  constructor(private route: ActivatedRoute, private taskService: TaskService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['name'];
    });

    this.route.queryParams.subscribe((params: Params) => {
      this.tags = params['tags'];
      this.actualEffort = params['actual_effort'];
      this.estimatedEffort = params['estimated_effort'];
      this.dueDate = params['due_date'];
      this.description = params['description'];
    });
  }

  editTask() {
    this.editTaskStatus = true;
    this.editTaskValue = this.editTaskInput.nativeElement.value;
    window.setTimeout(() => {
      this.editTaskInput.nativeElement.focus();
    }, 0);
  }

  blurTask() {
    const taskValue = this.editTaskInput.nativeElement.value;
    this.editTaskStatus = false;
    if (taskValue !== this.editTaskValue && taskValue !== '') {
      this.name = taskValue;

      this.tempTasks = JSON.parse(localStorage.getItem('tasks'));
      this.tempTasks.filter((t) => {
        if (+t.id === +this.id) {
          t.name = taskValue;
        }
      });
      localStorage.setItem('tasks', JSON.stringify(this.tempTasks));

      this.editTaskValue = '';
      this.sub = this.taskService.editTask({
        id: this.id,
        name: this.name,
        tags: this.tags,
        actual_effort: this.actualEffort,
        estimated_effort: this.estimatedEffort,
        due_date: this.dueDate,
        description: this.description
      }).subscribe((data) => console.log(data));
    }
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
