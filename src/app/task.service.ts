import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) {}

  public getTasks(): Observable<any> {
    return this.http.get('./assets/tasks.json');
  }

  public editTask(task): Observable<any> {
     return this.http.put(`http://localhost:3000/tasks/${task.id}`, task);
  }
}
